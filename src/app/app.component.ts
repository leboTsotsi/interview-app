import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent {
  // todo: map from button component
  public disabled: boolean = false;

  public buttonClicked(): void {
    alert('Button Click');
  }
}
